<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        $owners = array_unique(array_values($files));

        $groupByOwner = array_map(function ($owner) use ($files) {
            $filesByOwner = array_filter(
                $files,
                function ($name) use ($owner) {
                    return $name == $owner;
                }
            );

            return array(
                array_keys($filesByOwner),
                'owner' => $owner
            );
        }, $owners);

        return array_column($groupByOwner, 0, 'owner');
    }
}

$files = array(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));
