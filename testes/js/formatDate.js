// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

function formatDate(userDate) {
  const date = new Date(userDate);
  const options = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit'
  };

  return date.toLocaleString('en-us', options)
    .replace(/(\d+)\/(\d+)\/(\d+)/, '$3$1$2');
}

console.log(formatDate("12/31/2014"));